//
//  ViewController.swift
//  Demo Google Maps
//
//  Created by Click Labs on 2/6/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    var marker1 = GMSMarker()
    var manager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        // 30.719057, 76.810401
        
        self.mapView.delegate = self
        
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        
        var camera = GMSCameraPosition.cameraWithLatitude(30.719057,
            longitude: 76.810401, zoom: 15)
        mapView.camera = GMSCameraPosition.cameraWithLatitude(30.719057,
            longitude: 76.810401, zoom: 15)
        //mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
        mapView.myLocationEnabled = true
        
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        
        self.mapView.setMinZoom(10, maxZoom: 25)
        //self.view = mapView
        
        var marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(30.719057, 76.810401)
        marker.title = "Click Labs"
        marker.snippet = "My Office"
        marker.map = mapView
      
      

        
    }
    

    func mapView(mapView: GMSMapView!, didLongPressAtCoordinate coordinate: CLLocationCoordinate2D) {
        
        
        
        GMSGeocoder().reverseGeocodeCoordinate(coordinate){
            (response, error) in
            if let address = response?.firstResult() {
            
            let r = GMSReverseGeocodeResponse()
            
            self.marker1.position = coordinate
            self.marker1.appearAnimation = kGMSMarkerAnimationPop
            self.marker1.map = self.mapView
            self.marker1.title = "\(address.thoroughfare)\n\(address.subLocality)"
            self.marker1.snippet = address.administrativeArea + ", " + address.country + "(\(address.postalCode))"
            var cameraUpdate = GMSCameraUpdate.setTarget(coordinate)
            self.mapView.animateWithCameraUpdate(cameraUpdate)
 
            
            
            }
        }
    }

    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    /*
    func mapView(mapView: GMSMapView!, markerInfoWindow marker: GMSMarker!) -> UIView! {
        var DynamicView=UIView(frame: CGRectMake(100, 200, 100, 100))
        DynamicView.backgroundColor=UIColor.greenColor()
        DynamicView.layer.cornerRadius=25
        DynamicView.layer.borderWidth=2
        //self.view.addSubview(DynamicView)
      
        return DynamicView
    }
    */

    @IBOutlet weak var mapType: UISegmentedControl!
    
    @IBAction func changeMapType(sender: AnyObject) {
        
        switch(mapType.selectedSegmentIndex){
        case 0:
            mapView.mapType = kGMSTypeNormal
            
        case 1:
            mapView.mapType = kGMSTypeSatellite
            
        case 2:
            mapView.mapType = kGMSTypeHybrid
            
        default:
            println("")
        }
        
    }
}

